------------------------------------------------
--               Apo-Soccer                   --
--                                            --
--        Programmierwettbewerb 2009          --
--     der Otto-von-Guericke-Universit�t      --
--                Magdeburg                   --
------------------------------------------------

Bitte ladet NICHT die AIMultiplexer.dll, da sonst das Programm einfach abst�rzt.
Diese dll ist die Schnittstelle zu den anderen dll-KI's.

Desweiteren findet ihr die ganzen vorgegebenen Spieler in den Unterordnern.
Um einen Spieler zu laden geht einfach im Menu auf loadHomeAI oder loadVisitorAI
und w�hlt die class bzw. dll Datei aus. Bei den Steto-Spielern ist die Startklasse,
die Steto.class .

F�r weitere Fragen schaut euch bitte auf www.apo-games.de/apoSoccer/ um. Dort stehen zu 99%
die L�sungen zu euren Fragen, entweder direkt auf der Site oder aber im Forum.

Wenn ihr zu Debugzwecken den Ball steuern wollt, dann ladet die Ghost KI. Im Spiel k�nnt ihr dann
den Ball mit den Pfeiltasten steuern und mit 's' abstoppen.

Wenn ihr auch Pause dr�ckt, dann k�nnt ihr durch klicken auf das Spielfeld einen Schritt hinzuf�gen.
Sehr gut zu Debugzwecken. Also Pause und danach ins Programm klicken und schon startet der n�chste Schritt
und so k�nnt ihr Schritt f�r Schritt das Spiel durchgehen und so hoffentlich auch die letzten Fehler finden. =)
Oder ihr stellt einfach im Pausemenu ein, ob das Spiel 10 oder 100 mal langsamer ablaufen soll.

Wenn ihr ein Replay abspielt, k�nnt ihr auch den Speed einstellen. Ihr k�nnt euch zwischen 4 mal so schnell
wie normal zur�ckspulen, oder 2 mal so schnell zur�ck, Pause, normaler Speed und 2 bzw 4 mal so schnell vorw�rts
spulen.


Der Sourcecode liegt mit in der Jar-Datei. Wenn ihr diese in Eclipse oder Netbeans einf�gt, dann k�nnt ihr gleich 
zur entsprechenden Klasse springen. Interessant f�r euch sind vor allem die Berechnungen in der Klasse ApoBall und ApoPlayer.


Wir w�nschen euch viel Spa� beim Entwickeln einer KI. =)